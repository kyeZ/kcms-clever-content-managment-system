<?php

class dispatcher
{
	public static function dispatch($router){
		
		$config = registry::register('config');	
		
			$controller = $router->getController();
			$action = $router->getAction();
			$params = $router->getParams();
		
		$controller_path = $config->controllers_path;
		$controller_file = $controller_path.$controller.'.php';
		
		if(!file_exists($controller_file))
		{	
			$error = registry::register('sgExeption');
			$error->throwExeption('Nie mozna ZNALEŚĆ KONTROLERA');
			
			//echo "NIE MOŻNA ZNALEŚĆ PLIKU KONTROLERA";
			
		}
		
		include_once($controller_file);
		
		$sys = new $controller($params);
		$sys->$action();
		
		$view = registry::register('view');
		
		if(!empty($sys->template)) $view->setTemplate($sys->template);
		
		$template = $view->getTemplate($action);
		
		main::templateLoader($controller, $template);
	}
}

?>