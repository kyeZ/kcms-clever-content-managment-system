<?php




/**
 *@author Przemysław Urbaniak (kyeZ)
 *@website www.kyez.za.pl
 *-----------------------------------
 *
 *Klasa registry 
 *
 *Klasa wzorca Singleton
 * 
 *Klasa regiatry posiada jedną publiczą statyczną
 *metodę - register. Odpowiedzialna jest za 
 *spradzanie czy obiekt podany w parametrze został
 *już wcześniej powołany do życia. Metoda zwraca obiekt 
 *klasy podany w parametrze i zapisuje ten obiekt
 *do prywatnego pola $regitered aby móc później zwrócić
 *w przypadku ponoweego wywołania tego obiektu do życia
 *ten sam obiekt bez tworzenia nowego egemplarza.
 */


class registry
{
	private static $registered = Array();
	
	public static function register($object, $params = false)
	{
		if(empty(self::$registered[$object]))
		{
			if(!$params)
			{
				self::$registered[$object] = new $object();
			}
			else
			{
				self::$registered[$object] = new $object($params);
			}			
		}		
		
		return self::$registered[$object];
				
	}
	
}

?>