<?php

class view
{
	private $template;
	
	public function setTemplate($template)
	{
		$this->template = $template;
	}
	
	public function getTemplate($controller)
	{
		return (empty($this->template)) ? $controller : $this->template;
	}
	
}


?>