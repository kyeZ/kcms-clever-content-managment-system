<?php
class router
{
	private $controller;
	private $action;
	private $params;
	
	public function __construct()
	{
		$config = registry::register("config");
		
		if(!isset($_GET['page']))
		{
			$path = $config->default_controller;
		}
		else
		{	
			$path = $_GET['page'];		
		}

		$parts = explode("/", $path);
		
		$this->controller = $parts[0];
		$this->action = (isset($parts[1])) && $parts[1] != "" ? $parts[1] : "index";
		
		array_shift($parts);	
		array_shift($parts);
		
		$this->params = $parts;
	}
	
	public function getController()
	{
		return $this->controller;
	}
	
	public function getAction()
	{	
	 	return $this->action;
	}
	
	public function getParams()
	{
		if(isset($_POST) && count($_POST) > 0)
		{
			foreach($_POST as $key => $val)
			{
				$this->params['POST'][$key] = $val;
			}
		}
		
		return $this->params;
	}
}

?>