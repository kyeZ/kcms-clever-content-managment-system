<?php
class main
{
	
	public function __get($name)
	{
		$nameParts = explode("_", $name);
		$helperName = $nameParts[0].'_'.$nameParts[1].'.php';
		
		$type = $nameParts[1];
		
		include_once $helperName;
		
		if(!defined(strtoupper($nameParts[0]))){
			echo "BŁĄD WCZYTYWANIA PLIKU".$helperName;
			exit();
		}
		
	}
	
	public static function templateLoader($controller, $template)
	{
		$config = registry::register('config');
		$templatefile = $config->views_path.$controller.'/'.$template.'.php';

		if(!file_exists($templatefile))
		{
			echo "WIDOK ".$template." JEST NIEDOSTĘPNY W LOKALIZACJI: ".$config->views_path.$controller."/";
			exit();
		}
		
		include_once($templatefile);
				
	}	
	
}


?>