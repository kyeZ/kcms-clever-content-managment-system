<?php

if(!defined('DS')) define('DS', '/');

$AbsoluteURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
$AbsoluteURL .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
$slash = substr($AbsoluteURL, -1);
$NewURL = $slash != '/' ? $AbsoluteURL.'/' : $AbsoluteURL;

define('SERVER_ADDRESS', $NewURL);


$configs['site_title'] = 'kCMS - Clever Content Managment System';

/*
 *  SYSTEM SETTINGS - DIRECTORIES
 */


$configs['default_controller'] = 'home';

$configs['controllers_path'] = 'app/controllers/';
$configs['models_path'] = 'app/models/';
$configs['views_path'] = 'app/views/';

/*
 * SYSTEM SETTINGS - DATABASE
 */

$config['db_host'] = 'localhost';
$config['db_user'] = 'root';
$config['db_pass'] = '';
$config['db_db_name'] = 'kCMS';





?>